<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Jobs\SendReminderEmail;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

/**
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="Swagger documented API",
 *     version="2.0"
 *   )
 * )
 */
class TaskController extends Controller
{
    /**
     * @SWG\Get(path="/tasks",
     *   summary="Gets a list of task",
     *   operationId="taskList",
     *   produces={"application/json"},
     *   parameters={},
     *   @SWG\Response(
     *     response=200,
     *     description="A list of task",
     *     @SWG\Schema(
     *
     *     )
     *   )
     * )
     */
    public function taskList()
    {
        $tasks = Task::orderBy('created_at', 'asc')->get();

        return Response::json(array(
            'tasks' => $tasks
        ));
    }


    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required|max:255']);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $task = new Task;
        $task->name = $request->name;
        $task->save();

        return redirect('/');
    }


    //
    public function index()
    {
        for ($i = 0; $i <= 1000000; $i++) {
            $this->dispatch(new SendReminderEmail("hello world rabbitmq" . microtime()));
        }
    }

}
