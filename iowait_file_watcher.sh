#!/bin/sh
## http://superuser.com/questions/956311/continuously-detect-new-files-with-inotify-tools-within-multiple-directories-r
MONITORDIR="./database"
inotifywait -m -r -e create --format '%w%f' "${MONITORDIR}" | while read NEWFILE
do
   echo "Updating permission for ${NEWFILE}"
   chmod 777 ${NEWFILE}
done

